module.exports = function (gulp, plugins, path) {
	return () => {

		function getConn() {
			return plugins.ftp.create({
				host: 'universe.artbayard.ru',
				user: 'universe.artbayard.ru|ftp_universe',
				pass: '12341234'
			});
		}

		var conn = getConn();

		return gulp.src(path.public.all)
			.pipe(conn.dest('./'));
	}
}