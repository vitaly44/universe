module.exports = function (gulp, plugins, path) {
	return () => {
		return gulp.src(path.public.cssAll)
			.pipe(plugins.plumber({
				errorHandler: plugins.notify.onError()
			}))
			.pipe(plugins.cleanCSS())
			.pipe(
				plugins.rename({
					suffix: '.min'
				})
			)
			.pipe(gulp.dest(path.public.css))
			.pipe(plugins.connect.reload());
	}
}